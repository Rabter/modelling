#include <cstdio>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow), x(0), t(0), Tt(0), Tx(0)
{
//    create_step_x_table();
//    create_step_t_table();
    ui->setupUi(this);
    ui->graphic_T_x->chart()->legend()->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::add_series(const std::vector<double> &x, const std::vector<double> &y, QChart *graphic)
{
    unsigned size = x.size();
    if (size == y.size())
    {
        QLineSeries *series = new QLineSeries();
        for (unsigned i = 0; i < size; ++i)
            series->append(x[i], y[i]);
        graphic->addSeries(series);
    }
}

void MainWindow::set_series(const std::vector<double> &x, const std::vector<double> &y, QChart *graphic)
{
    graphic->removeAllSeries();
    add_series(x, y, graphic);
}

void MainWindow::set_Fxt(const std::vector<double> &x, const std::vector<std::vector<double> > &T)
{
    if (x.size() && T.size())
    {
        ui->graphic_T_x->chart()->removeAllSeries();
        for (const std::vector<double> &Tcur: T)
            add_series(x, Tcur, ui->graphic_T_x->chart());

        ui->graphic_T_x->chart()->createDefaultAxes();
    }
}

void MainWindow::set_Fx(const std::vector<double> &x, const std::vector<double> &scale, const std::vector<std::vector<double> > &Tx)
{
    if (x.size() && Tx.size())
    {
        ui->graphic_T_x->chart()->removeAllSeries();
        int index = ui->horizontalSlider->value() * (scale.size() - 1) / ui->horizontalSlider->maximum();
        add_series(x, Tx[index], ui->graphic_T_x->chart());

        ui->graphic_T_x->chart()->createDefaultAxes();
    }
}

void MainWindow::calc_Tt()
{
    unsigned Tx_size = Tx.size();
    if (Tx_size)
    {
        unsigned Tt_size = Tx[0].size();
        Tt.clear();
        Tt.resize(Tt_size);
        for (unsigned i = 0; i < Tx_size; ++i)
            for (unsigned j = 0; j < Tt_size; ++j)
                Tt[j].push_back(Tx[i][j]);
    }
}

void MainWindow::update_chart()
{
    if (ui->rb_Fxt->isChecked())
        set_Fxt(x, Tx);
    else if (ui->rb_Fx->isChecked())
        set_Fx(x, t, Tx);
    else if (ui->rb_Ft->isChecked())
        set_Fx(t, x, Tt);
}

void MainWindow::on_btn_calculate_clicked()
{
    bool correct;
    double frequency;

    correct = ui->entry_l->get_double(rod.l);
    correct = ui->entry_R->get_double(rod.R) && correct;
    correct = ui->entry_fmax->get_double(rod.Fmax) && correct;
    correct = ui->entry_tmax->get_double(rod.tmax) && correct;
    correct = ui->entry_a0->get_double(rod.emission0) && correct;
    correct = ui->entry_aN->get_double(rod.emissionN) && correct;
    correct = ui->entry_tenv->get_double(rod.Tenv) && correct;
    correct = ui->entry_h->get_double(rod.dx) && correct;
    correct = ui->entry_t->get_double(rod.dt) && correct;
    correct = ui->entry_frequency->get_double(frequency) && correct;
    rod.period = 1 / frequency;

    correct = ui->entry_a1->get_double(rod.a1) && correct;
    correct = ui->entry_b1->get_double(rod.b1) && correct;
    correct = ui->entry_c1->get_double(rod.c1) && correct;
    correct = ui->entry_m1->get_double(rod.m1) && correct;
    correct = ui->entry_a2->get_double(rod.a2) && correct;
    correct = ui->entry_b2->get_double(rod.b2) && correct;
    correct = ui->entry_c2->get_double(rod.c2) && correct;
    correct = ui->entry_m2->get_double(rod.m2) && correct;

    if (correct)
    {
        rod.calulate(x, t, Tx);
        calc_Tt();
        update_chart();
//        ui->graphic_T_x->chart()->axes(Qt::Vertical)[0]->setRange(0, 400); // Usefull if F0 = 0 (otherwise the chart goes crazy because the range is too short)
    }
}

void MainWindow::create_step_x_table()
{
    FILE *out = fopen("dx.txt", "w");
    std::vector<std::vector<double>> res;

    std::vector<std::vector<double>> table;
    std::vector<double> hs;
    std::vector<double> xs;
    std::vector<double> tmp1, tmp2;

    rod.dt = 1e-1;
    for (double h = 0.0001; h < 1.1; h *= 10)
    {
        std::vector<double> temperatures;
        rod.dx = h;
        hs.push_back(h);
        rod.calulate(tmp1, tmp2, res);
        std::vector<double> certain_res = res[res.size() - 1];
        xs.clear();
        for (double x = 0; x < 10; x += 0.2)
        {
            int index = (int)(x / h);
            xs.push_back(x);
            temperatures.push_back(certain_res[index]);
        }
        table.push_back(temperatures);
    }

    fprintf(out, " x  ");
    for (double h: hs)
        fprintf(out, "|  %6.4lf   ", h);
    fprintf(out, "\n");

    for (unsigned i = 0; i < table[0].size(); ++i)
    {
        fprintf(out, "%3.1lf ", xs[i]);
        for (unsigned j = 0; j < table.size(); ++j)
            fprintf(out, "|%10.6lf ", table[j][i]);
         fprintf(out, "\n");
    }
    fclose(out);
}

void MainWindow::create_step_t_table()
{
    FILE *out = fopen("dt.txt", "w");

    std::vector<std::vector<double>> res;
    std::vector<std::vector<double>> table;
    std::vector<double> xs;
    std::vector<double> ts;
    std::vector<double> tmp1, tmp2;

    rod.dx = 1e-3;
    for (double t = 0.0001; t < 1.1; t *= 10)
    {
        std::vector<double> temperatures;
        rod.dt = t;
        ts.push_back(t);
        rod.calulate(tmp1, tmp2, res);
        std::vector<double> certain_res = res[res.size() - 1];
        xs.clear();
        for (double x = 0; x < 10; x += 0.2)
        {
            int index = (int)(x / rod.dx);
            xs.push_back(x);
            temperatures.push_back(certain_res[index]);
        }
        table.push_back(temperatures);
    }

    fprintf(out, " x  ");
    for (double t: ts)
        fprintf(out, "|  %6.4lf   ", t);
    fprintf(out, "\n");

    for (unsigned i = 0; i < table[0].size(); ++i)
    {
        fprintf(out, "%3.1lf ", xs[i]);
        for (unsigned j = 0; j < table.size(); ++j)
            fprintf(out, "|%10.6lf ", table[j][i]);
         fprintf(out, "\n");
    }
    fclose(out);
}

void MainWindow::on_rb_Fxt_clicked()
{
    ui->horizontalSlider->setDisabled(true);
    update_chart();
}

void MainWindow::on_rb_Fx_clicked()
{
    ui->horizontalSlider->setEnabled(true);
    update_chart();
}

void MainWindow::on_rb_Ft_clicked()
{
    ui->horizontalSlider->setEnabled(true);
    update_chart();
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    update_chart();
    (void)value;
}
