#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QChart>
#include "rod.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_calculate_clicked();
    void on_rb_Fxt_clicked();
    void on_rb_Fx_clicked();
    void on_rb_Ft_clicked();

    void on_horizontalSlider_valueChanged(int value);

private:
    void add_series(const std::vector<double> &x, const std::vector<double> &y, QT_CHARTS_NAMESPACE::QChart *graphic);
    void set_series(const std::vector<double> &x, const std::vector<double> &y, QT_CHARTS_NAMESPACE::QChart *graphic);

    void set_Fxt(const std::vector<double> &x, const std::vector<std::vector<double>> &T);
    void set_Fx(const std::vector<double> &x, const std::vector<double> &scale, const std::vector<std::vector<double> > &Tx);

    void calc_Tt();
    void update_chart();

    void create_step_x_table();
    void create_step_t_table();

    Ui::MainWindow *ui;
    Rod rod;

    std::vector<double> x, t;
    std::vector<std::vector<double>> Tt, Tx;
};
#endif // MAINWINDOW_H
