#include "interpolate.h"

double interpolate(std::vector<double> key_arr, std::vector<double> val_arr, double key)
{
    unsigned ikey;
    if (key < key_arr[1])
        ikey = 1;
    else if (key > key_arr[key_arr.size() - 2])
        ikey = key_arr.size() - 1;
    else
    {
        unsigned i = 1;
        while (key > key_arr[i])
            ++i;
        ikey = i;
    }
    return val_arr[ikey - 1] + (val_arr[ikey] - val_arr[ikey - 1]) * (key - key_arr[ikey - 1]) / (key_arr[ikey] - key_arr[ikey - 1]);
}
