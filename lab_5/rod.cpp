#include <cmath>
#include <QDebug>
#include "rod.h"
#include "run_through.h"
#include "interpolate.h"
#include "mode.h"
#define ABS(x) ((x) > 0? (x): -(x))
#define MAX_DIFF_ALLOWED 1e-3
#define BALANCE_ERROR 1e-2
#define MAX_TIME 50

Rod::Rod()
{
    l = 10;
    R = 0.5;
    Tenv = 300;
    Fmax = 50;
    tmax = 1;
    emission0 = 0.05;
    emissionN = 0.01;
    dx = 1e-3;
    dt = 1e-1;

    a1 = 0.0134;
    b1 = 1;
    c1 = 4.35e-4;
    m1 = 1;
    a2 = 2.049;
    b2 = 0.563e-3;
    c2 = 0.528e5;
    m2 = 1;
}

void Rod::calulate(std::vector<double> &x_vec, std::vector<double> &t_vec, std::vector<std::vector<double>> &T)
{
    constexpr double x0 = 0;
    run_through_paramseters params;

    size = lround(l / dx);
    Tcur = Tprev = std::vector<double>(size, Tenv);
    x = std::vector<double>(size);
    t_cur = dt;
    double xcur = 0;
    for (unsigned i = 0; i < size; ++i)
    {
        x[i] = xcur;
        xcur += dx;
    }
    x_vec = x;
    t_vec.clear();
    T.clear();
    std::vector<double> Tp1;
    do
    {
        Tprev = Tcur;
        do
        {
            Tp1 = Tcur;
            double X = KHI(Tcur[0], Tcur[1]);
            double p = (emission0 + emission(x0 + dx)) / R; // (p0 + p1) / 2
            double f = (emission0 + emission(x0 + dx)) * Tenv / R; // (f0 + f1) / 2
            double C0 = capacity(Tcur[0]), Ch = (capacity(Tcur[0]) + capacity(Tcur[1])) / 2, CN = capacity(Tcur[Tcur.size() - 1]);
            params.K0 = dt * ((X / dx) + (dx * p / 8) + (dx * emission0 / (R * 2))) + (C0 * dx / 4) + (Ch * dx / 8);
            params.M0 = dt * ((dx * p / 8) - (X / dx)) + (Ch * dx / 8);
            params.P0 = dt * (F0(t_cur) + (dx * (f + 2 * emission0 * Tenv / R) / 4)) + (C0 * interpolate(x, Tprev, x0) * dx / 4) + (Ch * (interpolate(x, Tprev, x0) + interpolate(x, Tprev, x0 + dx)) * dx / 8);

            X = KHI(Tcur[Tcur.size() - 2], Tcur[Tcur.size() - 1]);
            p = (emissionN + emission(l - dx)) / R; // (Pn-1 + Pn) / 2
            f = (emissionN + emission(l - dx)) * Tenv / R; // (Fn-1 + Fn) / 2
            Ch = (capacity(Tcur[Tcur.size() - 1]) + capacity(Tcur[Tcur.size() - 2])) / 2;

            params.KN = -dt * ((X / dx) + emissionN + (dx * p / 8) + (dx * emissionN / (R * 2))) + (CN * dx / 4) + (Ch * dx / 8);
            params.MN = dt * ((X / dx) - (dx * p / 8)) + (Ch * dx / 8);
            params.PN = -dt * ((emissionN * Tenv) + (dx * (f + (2 * emissionN * Tenv / R)) / 4)) + (CN * Tprev[size - 1] * dx / 4) + (Ch * (Tprev[size - 1] + Tprev[size - 2]) * dx / 8);
            // Tprev in point l is always the last value so interpolation is only slowing down the program. x0 can be diferent from 0 and if it is not, the interpolation function will return on the 1st iteration

            params.resize(size);
            unsigned sizedec = size - 1;
            for (unsigned i = 1; i < sizedec; ++i)
            {
                double A = dt * KHI(Tcur[i - 1], Tcur[i]) / dx;
                double C = dt * KHI(Tcur[i], Tcur[i + 1]) / dx;
                params.A[i] = A;
                params.B[i] = A + C + 2 * dt * dx * emission(x[i]) / R + capacity(Tcur[i]) * dx;
                params.C[i] = C;
                params.D[i] = 2 * dt * dx * Tenv * emission(x[i]) / R + capacity(Tcur[i]) * dx * Tprev[i];
            }
            params.A[0] = params.A[1];
            params.B[0] = params.B[1];
            params.C[0] = params.C[1];
            params.D[0] = params.D[1];
            params.A[sizedec] = params.A[size - 2];
            params.B[sizedec] = params.B[size - 2];
            params.C[sizedec] = params.C[size - 2];
            params.D[sizedec] = params.D[size - 2];

            run_through(Tcur, params);
        } while(max_diff(Tcur, Tp1) > MAX_DIFF_ALLOWED);

        T.push_back(Tcur);
        t_vec.push_back(t_cur);
        t_cur += dt;
#ifdef frequency_mode
    } while(t_cur < MAX_TIME);
#else
    } while(max_diff(Tcur, Tprev) > MAX_DIFF_ALLOWED);
#endif

    if (power_is_balanced())
        qDebug() << "Power is balanced\n";
    else
        qDebug() << "Power is not balanced\n";
}

double Rod::conductivity(double T)
{
    return a1 * (b1 + c1 * pow(T, m1));
}

double Rod::emission(double x)
{
    double b = emissionN * l / (emissionN - emission0);
    double a = -emission0 * b;
    return a / (x - b);
}

double Rod::capacity(double T)
{
    return a2 + b2 * pow(T, m2) - c2 / (T * T);
}

double Rod::KHI(double Tl, double Tr)
{
    double k1 = conductivity(Tl);
    double k2 = conductivity(Tr);

    return 2 * k1 * k2 / (k1 + k2);
}

double Rod::max_diff(const std::vector<double> &T1, const std::vector<double> &T2)
{
    double max = 0;
    for (unsigned i = 0; i < size; ++i)
    {
        double diff = ABS(T1[i] - T2[i]);
        if (diff > max)
            max = diff;
    }
    return max;
}

double Rod::rectangle_impulse(double t)
{
    if (t < tmax)
        return Fmax;
    return 0;
}

double Rod::exponential_impulse(double t)
{
    return (Fmax * t / tmax) * exp(1 - t / tmax);
}

// Implementation depends on chosen mode (mode.h)
#ifdef constant_mode
double Rod::F0(double t)
{
    return Fmax;
    (void) t;
}
#endif

#ifdef casual_mode
double Rod::F0(double t)
{
    return exponential_impulse(t);
}
#endif

#ifdef frequency_mode
double Rod::F0(double t)
{
    double Ftotal = 0;
    unsigned impulses_past = static_cast<unsigned>(t / period);
    for (unsigned i = 0; i <= impulses_past; ++i)
    {
        double impulse_time = t - i * period;
        Ftotal += exponential_impulse(impulse_time);
    }
    return Ftotal;
}
#endif

bool Rod::power_is_balanced()
{
    double FN = emissionN * (Tcur[Tcur.size() - 1] - Tenv);
    double integral = 0;
    for (unsigned i = 1; i < Tcur.size(); ++i)
    {
        double lb = emission((i - 1) * dx) * (Tcur[i - 1] - Tenv);
        double rb = emission(i * dx) * (Tcur[i] - Tenv);
        integral += lb + rb;
    }
    integral *= dx / 2;

    double top = F0(t_cur) - FN;
    double bottom = 2 * integral / R;
    double tmp = ABS(top / bottom - 1);
    return tmp < BALANCE_ERROR;
}

