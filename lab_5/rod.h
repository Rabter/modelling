#ifndef ROD_H
#define ROD_H

#include <vector>

class Rod
{
public:
    Rod();
    void calulate(std::vector<double> &x, std::vector<double> &t_vec, std::vector<std::vector<double> > &T);

    double l, R, Tenv, Fmax, tmax, emission0, emissionN, dx, dt, period;

    double a1, b1, c1, m1, a2, b2, c2, m2;

private:
    double conductivity(double T);
    double emission(double x);
    double capacity(double T);
    double KHI(double Tl, double Tr);

    double max_diff(const std::vector<double> &T1, const std::vector<double> &T2);

    double F0(double t);

    bool power_is_balanced();

    unsigned size;
    std::vector<double> Tprev, Tcur, x;
    double t_cur;

    double rectangle_impulse(double t);
    double exponential_impulse(double t);
};

#endif // ROD_H
