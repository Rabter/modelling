#include "run_through.h"

void run_through(std::vector<double> &res, run_through_paramseters &params)
{
    unsigned size = params.size;
    std::vector<double> ksi(size), etta(size);
    ksi[0] = -params.M0 / params.K0;
    etta[0] = params.P0 / params.K0;

    unsigned idec = 0;
    for (unsigned i = 1; i < size; ++i)
    {
        ksi[i] = params.C[idec] / (params.B[idec] - params.A[idec] * ksi[idec]);
        etta[i] = (params.D[idec] + params.A[idec] * etta[idec]) / (params.B[idec] - params.A[idec] * ksi[idec]);
        idec = i;
    }

    res.resize(size);
    res[size - 1] = (params.PN - params.MN * etta[size - 1]) / (params.KN + params.MN * ksi[size - 1]);
    idec = size - 2;
    for (unsigned i = size - 1; i > 0; i = idec--)
        res[idec] = ksi[i] * res[i] + etta[i];
}
