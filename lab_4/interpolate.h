#ifndef INTERPOLATE_H
#define INTERPOLATE_H

#include <vector>

double interpolate(std::vector<double> key_arr, std::vector<double> val_arr, double key);

#endif // INTERPOLATE_H
