#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphic_T_x->chart()->legend()->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::add_series(const std::vector<double> &x, const std::vector<double> &y, QChart *graphic, const QColor &color)
{
    unsigned size = x.size();
    if (size == y.size())
    {
        QLineSeries *series = new QLineSeries();
        for (unsigned i = 0; i < size; ++i)
            series->append(x[i], y[i]);
        series->setColor(color);
        graphic->addSeries(series);
    }
}

void MainWindow::set_series(const std::vector<double> &x, const std::vector<double> &y, QChart *graphic, const QColor &color)
{
    graphic->removeAllSeries();
    add_series(x, y, graphic, color);
}

void MainWindow::on_btn_calculate_clicked()
{
    bool correct;

    correct = ui->entry_l->get_double(rod.l);
    correct = ui->entry_R->get_double(rod.R) && correct;
    correct = ui->entry_f0->get_double(rod.F0) && correct;
    correct = ui->entry_k0->get_double(rod.cond0) && correct;
    correct = ui->entry_kN->get_double(rod.condN) && correct;
    correct = ui->entry_a0->get_double(rod.emission0) && correct;
    correct = ui->entry_aN->get_double(rod.emissionN) && correct;
    correct = ui->entry_tenv->get_double(rod.Tenv) && correct;
    correct = ui->entry_h->get_double(rod.h) && correct;

    if (correct)
    {
        std::vector<double> x, T;

        rod.calulate(x, T);
        set_series(x, T, ui->graphic_T_x->chart(), Qt::blue);

//        rod.emission0 *= 3;  // uncomment to compare graphics with different emission
//        rod.emissionN *= 3;
//        rod.calulate(x, T);
//        add_series(x, T, ui->graphic_T_x->chart(), Qt::red);

        ui->graphic_T_x->chart()->createDefaultAxes();
//        ui->graphic_T_x->chart()->axes(Qt::Vertical)[0]->setRange(0, 400); // Usefull if F0 = 0 (otherwise the chart goes crazy because the range is too short)
    }
}
