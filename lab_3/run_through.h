#ifndef RUN_THROUGH_H
#define RUN_THROUGH_H

#include <vector>

struct run_through_paramseters
{
    run_through_paramseters(): A(0), B(0), C(0), D(0) {}
    std::vector<double> A, B, C, D;
    double K0, M0, P0, KN, PN, MN;
};

void run_through(std::vector<double> &res, run_through_paramseters &params);

#endif // RUN_THROUGH_H
