#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QChart>
#include "rod.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_calculate_clicked();

private:
    void add_series(const std::vector<double> &x, const std::vector<double> &y, QT_CHARTS_NAMESPACE::QChart *graphic, const QColor &color = Qt::blue);
    void set_series(const std::vector<double> &x, const std::vector<double> &y, QT_CHARTS_NAMESPACE::QChart *graphic, const QColor &color = Qt::blue);

    Ui::MainWindow *ui;
    Rod rod;
};
#endif // MAINWINDOW_H
