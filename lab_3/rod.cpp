#include "rod.h"
#include "run_through.h"

Rod::Rod()
{
    l  = 10;
    R = 0.5;
    Tenv = 300;
    F0 = 100;
    cond0 = 0.1;
    condN = 0.2;
    emission0 = 1e-2;
    emissionN = 0.9e-2;
    h = 1e-6;
}

void Rod::calulate(std::vector<double> &x, std::vector<double> &T)
{
    constexpr double x0 = 0;
    run_through_paramseters params;
    x.resize(0);
    T.resize(0);

    double X = Xright(x0);
    double p = (emission0 + emission(x0 + h)) / R; // (p0 + p1) / 2
    double f = (emission0 + emission(x0 + h)) * Tenv / R; // (f0 + f1) / 2

    params.K0 = (X / h) + (h * p / 8) + (h * emission0 / (R * 2));
    params.M0 = (-X / h) + (h * p / 8);
    params.P0 = F0 + (h * (f + 2 * emission0 * Tenv / R) / 4);

    X = Xleft(l);
    p = (emissionN + emission(l - h)) / R; // (Pn-1 + Pn) / 2
    f = (emissionN + emission(l - h)) * Tenv / R; // (Fn-1 + Fn) / 2

    params.KN = (-X / h) - emissionN - (h * p / 8) - (h * emissionN / (R * 2));
    params.MN = (X / h) - (h * p / 8);
    params.PN = -(emissionN * Tenv) - (h * (f + (2 * emissionN * Tenv / R)) / 4);

    for (double xcur = x0; xcur < l; xcur += h)
    {
        double A = Xleft(xcur) / h;
        double C = Xright(xcur) / h;
        params.A.push_back(A);
        params.B.push_back(A + C + 2 * h * emission(xcur) / R);
        params.C.push_back(C);
        params.D.push_back(2 * h * Tenv * emission(xcur) / R);

        x.push_back(xcur);
    }

    run_through(T, params);
}

double Rod::conductivity(double x)
{
    double b = condN * l / (condN - cond0);
    double a = -cond0 * b;
    return a / (x - b);
}

double Rod::emission(double x)
{
    double b = emissionN * l / (emissionN - emission0);
    double a = -emission0 * b;
    return a / (x - b);
}

double Rod::Xleft(double x)
{
    return 2 * conductivity(x) * conductivity(x - h) / (conductivity(x) + conductivity(x - h));
}

double Rod::Xright(double x)
{
    return 2 * conductivity(x) * conductivity(x + h) / (conductivity(x) + conductivity(x + h));
}
