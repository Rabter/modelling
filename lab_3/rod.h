#ifndef ROD_H
#define ROD_H

#include <vector>

class Rod
{
public:
    Rod();
    void calulate(std::vector<double> &x, std::vector<double> &T);

    double l, R, Tenv, F0, cond0, condN, emission0, emissionN, h;

private:
    double conductivity(double x);
    double emission(double x);
    double Xleft(double x);
    double Xright(double x);
};

#endif // ROD_H
