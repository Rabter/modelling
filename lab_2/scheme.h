#ifndef SCHEME_H
#define SCHEME_H

#include <vector>

struct SchemeResult
{
    SchemeResult(): I(0), Uc(0), Rp(0), T0(0) {}
    std::vector<double> t, I, Uc, Rp, T0;
};

class Scheme
{
public:
    Scheme(const std::vector<double> *ITm, const std::vector<double> *Tsigma);

    static constexpr double R = 0.35, l = 12;
    double Tw = 0, Ck = 0, Lk = 0, Rk = 0, Uc0 = 0, I0 = 0;

    SchemeResult runge_2nd(double tb, double te, double h);
    SchemeResult runge_4th(double tb, double te, double h);

private:
    double calc_Rp(double I);
    std::vector<double> ITm[3];
    std::vector<double> Tsigma[2];

    double sigmaz(double I, double z);
    double calc_T0(double I);
    double T(double I, double z);

    double interpolate(std::vector<double> key_arr, std::vector<double> val_arr, double key);
    double integrate(double (Scheme::*f)(double I, double x), double I, double a, double b);

    double derivative_I(double I, double U);
    double derivative_U(double I);
};

#endif // SCHEME_H
