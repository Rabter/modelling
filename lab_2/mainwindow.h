#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QChartView>
#include "scheme.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_calculate_clicked();

    void on_rb_runge_2_clicked();

    void on_rb_runge_4_clicked();

private:
    Ui::MainWindow *ui;
    Scheme *scheme;
    SchemeResult (Scheme::*calculate)(double begin, double end, double step);

    int read_ITm(std::vector<double> *ITm, const char *filename);
    int read_Tsigma(std::vector<double> *Tsigma, const char *filename);
    void add_series(const std::vector<double> &x, const std::vector<double> &y, QT_CHARTS_NAMESPACE::QChart *graphic, const QColor &color = Qt::blue);
    void set_series(const std::vector<double> &x, const std::vector<double> &y, QT_CHARTS_NAMESPACE::QChart *graphic, const QColor &color = Qt::blue);
    void create_axes();
    void compare(double begin, double end, double step1, double step2);
};
#endif // MAINWINDOW_H
