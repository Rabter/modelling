#include <QFile>
#include <QTextStream>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#define ITm_FILE "../../ITm.txt"
#define TSIGMA_FILE "../../Tsigma.txt"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    std::vector<double> ITm[3], Tsigma[2];

    if (read_ITm(ITm, ITm_FILE) || read_Tsigma(Tsigma, TSIGMA_FILE))
        exit(1);

    ui->graphic_IRp_t->chart()->legend()->hide();
    ui->graphic_Uc_t->chart()->legend()->hide();
    ui->graphic_I_t->chart()->legend()->hide();
    ui->graphic_Rp_t->chart()->legend()->hide();
    ui->graphic_T0_t->chart()->legend()->hide();

    scheme = new Scheme(ITm, Tsigma);
    calculate = Scheme::runge_2nd;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete scheme;
}

int MainWindow::read_ITm(std::vector<double> *ITm, const char *filename)
{
    QFile fin(filename);
    if (!fin.open(QIODevice::ReadOnly))
    {
        qCritical("Cannot open ITm table file");
        return 1;
    }

    QTextStream in(&fin);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        QStringList list = line.split(' ');
        ITm[0].push_back(list[0].toDouble());
        ITm[1].push_back(list[1].toDouble());
        ITm[2].push_back(list[2].toDouble());
    }
    return 0;
}

int MainWindow::read_Tsigma(std::vector<double> *Tsigma, const char *filename)
{
    QFile fin(filename);
    if (!fin.open(QIODevice::ReadOnly))
    {
        qCritical("Cannot open Tsigma table file");
        return 1;
    }

    QTextStream in(&fin);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        QStringList list = line.split(' ');
        Tsigma[0].push_back(list[0].toDouble());
        Tsigma[1].push_back(list[1].toDouble());
    }
    return 0;
}

void MainWindow::add_series(const std::vector<double> &x, const std::vector<double> &y, QChart *graphic, const QColor &color)
{
    unsigned size = x.size();
    if (size == y.size())
    {
        QLineSeries *series = new QLineSeries();
        for (unsigned i = 0; i < size; ++i)
            series->append(x[i], y[i]);
        series->setColor(color);
        graphic->addSeries(series);
    }
}

void MainWindow::set_series(const std::vector<double> &x, const std::vector<double> &y, QChart *graphic, const QColor &color)
{
    graphic->removeAllSeries();
    add_series(x, y, graphic, color);
}

void MainWindow::create_axes()
{
    ui->graphic_IRp_t->chart()->createDefaultAxes();
    ui->graphic_Uc_t->chart()->createDefaultAxes();
    ui->graphic_I_t->chart()->createDefaultAxes();
    ui->graphic_Rp_t->chart()->createDefaultAxes();
    ui->graphic_T0_t->chart()->createDefaultAxes();
}

void MainWindow::compare(double begin, double end, double step1, double step2)
{
    SchemeResult res = scheme->runge_2nd(begin, end, step1);

    unsigned length = res.I.size();
    std::vector<double> IRp(length);
    for (unsigned i = 0; i < length; ++i)
        IRp[i] = res.I[i] * res.Rp[i];

    set_series(res.t, IRp, ui->graphic_IRp_t->chart(), Qt::red);
    res = scheme->runge_4th(begin, end, step2);

    length = res.I.size();
    IRp = std::vector<double>(length);
    for (unsigned i = 0; i < length; ++i)
        IRp[i] = res.I[i] * res.Rp[i];

    add_series(res.t, IRp, ui->graphic_IRp_t->chart());

    create_axes();
}

void MainWindow::on_btn_calculate_clicked()
{
    double begin, end, step;
    bool correct;

    correct = ui->entry_ck->get_double(scheme->Ck);
    correct = ui->entry_i0->get_double(scheme->I0) && correct;
    correct = ui->entry_lk->get_double(scheme->Lk) && correct;
    correct = ui->entry_rk->get_double(scheme->Rk) && correct;
    correct = ui->entry_tw->get_double(scheme->Tw) && correct;
    correct = ui->entry_uc0->get_double(scheme->Uc0) && correct;
    correct = ui->entry_begin->get_double(begin) && correct;
    correct = ui->entry_end->get_double(end) && correct;
    correct = ui->entry_step->get_double(step) && correct;

    if (correct)
    {
//        compare(begin, end, 1e-5, step);
//        return;
        SchemeResult res = (scheme->*calculate)(begin, end, step);

        unsigned length = res.I.size();
        std::vector<double> IRp(length);
        for (unsigned i = 0; i < length; ++i)
            IRp[i] = res.I[i] * res.Rp[i];

        set_series(res.t, IRp, ui->graphic_IRp_t->chart());
        set_series(res.t, res.Uc, ui->graphic_Uc_t->chart());
        set_series(res.t, res.I, ui->graphic_I_t->chart());
        set_series(res.t, res.Rp, ui->graphic_Rp_t->chart());
        set_series(res.t, res.T0, ui->graphic_T0_t->chart());

        create_axes();
    }
}

void MainWindow::on_rb_runge_2_clicked()
{
    calculate = Scheme::runge_2nd;
}

void MainWindow::on_rb_runge_4_clicked()
{
    calculate = Scheme::runge_4th;
}
