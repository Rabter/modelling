#include <cmath>
#include "scheme.h"

Scheme::Scheme(const std::vector<double> *ITm, const std::vector<double> *Tsigma)
{
    for (unsigned i = 0; i < 2; ++i)
    {
        this->ITm[i] = ITm[i];
        this->Tsigma[i] = Tsigma[i];
    }
    this->ITm[2] = ITm[2];
}

SchemeResult Scheme::runge_2nd(double tb, double te, double h)
{
    SchemeResult res;
    double I = I0, U = Uc0;
    for (double t = 0; t <= te; t += h)
    {
        if (t >= tb)
        {
            res.t.push_back(t);
            res.I.push_back(I);
            res.Uc.push_back(U);
            res.Rp.push_back(calc_Rp(I));
            res.T0.push_back(calc_T0(I));
        }

        double I1 = I + h * (derivative_I(I, U) + derivative_I(I + h * derivative_I(I, U), U + h * derivative_U(I))) / 2;
        U += h * (derivative_U(I) + derivative_U(I + h * derivative_I(I, U))) / 2;
        I = I1;
    }
    return res;
}

SchemeResult Scheme::runge_4th(double tb, double te, double h)
{
    SchemeResult res;

    double I = I0, U = Uc0;
    for (double t = 0; t <= te; t += h)
    {
        if (t >= tb)
        {
            res.t.push_back(t);
            res.I.push_back(I);
            res.Uc.push_back(U);
            res.Rp.push_back(calc_Rp(I));
            res.T0.push_back(calc_T0(I));
        }

        double k1 = h * derivative_I(I, U), m1 = h * derivative_U(I);
        double k2 = h * derivative_I(I + k1 / 2, U + m1 / 2), m2 = h * derivative_U(I + k1 / 2);
        double k3 = h * derivative_I(I + k2 / 2, U + m2 / 2), m3 = h * derivative_U(I + k2 / 2);
        double k4 = h * derivative_I(I + k3, U + m3), m4 = h * derivative_U(I + k3);

        I += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
        U += (m1 + 2 * m2 + 2 * m3 + m4) / 6;

    }
    return res;
}

double Scheme::calc_Rp(double I)
{
    constexpr double Rsqr = R * R;
    double integral = integrate(sigmaz, I, 0, 1);
    return l / (2 * M_PI * Rsqr * integral);
}

double Scheme::sigmaz(double I, double z)
{
    return interpolate(Tsigma[0], Tsigma[1], T(I, z)) * z;
}

double Scheme::calc_T0(double I)
{
    return interpolate(ITm[0], ITm[1], I);
}

double Scheme::T(double I, double z)
{
    double T0 = calc_T0(I), m = interpolate(ITm[0], ITm[2], I);
    return (Tw - T0) * pow(z, m) + T0;
}

double Scheme::interpolate(std::vector<double> key_arr, std::vector<double> val_arr, double key)
{
    unsigned ikey;
    if (key < key_arr[1])
        ikey = 1;
    else if (key > key_arr[key_arr.size() - 2])
        ikey = key_arr.size() - 1;
    else
    {
        unsigned i = 1;
        while (key > key_arr[i])
            ++i;
        ikey = i;
    }
    return val_arr[ikey - 1] + (val_arr[ikey] - val_arr[ikey - 1]) * (key - key_arr[ikey - 1]) /(key_arr[ikey] - key_arr[ikey - 1]);
}

double Scheme::integrate(double (Scheme::*f)(double I, double x), double I, double a, double b)
{
    double h = (b - a) / 100;
    double s1 = 0, s2 = 0;
    for (double x = a; x < b; x += h)
    {
        s1 += (this->*f)(I, x); //or use invoke (c++17)
        x += h;
        s2 += (this->*f)(I, x);
    }
    return h * ((this->*f)(I, a) + 4 * s1 + 2 * s2 + (this->*f)(I, b)) / 3;
}

double Scheme::derivative_I(double I, double U)
{
    return (U - (Rk + calc_Rp(I)) * I) / Lk;
}

double Scheme::derivative_U(double I)
{
    return -I / Ck;
}
