#include "doubleentry.h"

DoubleEntry::DoubleEntry(QWidget *parent): ValidEntry(parent)
{
    expression = "[+-]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)(?:e[+-]?\\d+)?";
}

bool DoubleEntry::get_double(double &num)
{
    if (validate())
    {
        make_correct();
        QString input = this->text();
        num = input.toDouble();
        return true;
    }
    else
    {
        make_inorrect();
        return false;
    }
}
