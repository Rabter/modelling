#include "polynom.h"
#define EPS 1e-10

void Polynom::add(const Member &member)
{
    if (member.first > EPS)
    {
        int checker = member.second;
        bool not_found = true;
        for (unsigned i = 0; i < addends.size() && not_found; ++i)
            if (addends[i].second == checker)
            {
                not_found = false;
                addends[i].first += member.first;
            }

        if (not_found)
            addends.push_back(member);
    }
}

void Polynom::integrate()
{
    for (Member &member: addends)
    {
        ++member.second;
        member.first /= member.second;
    }
}

void Polynom::square()
{
    Polynom res;
    unsigned size = addends.size();
    for (unsigned i = 0; i < size; ++i)
        for (unsigned j = 0; j < size; ++j)
            res.add(addends[i] * addends[j]);
    *this = res;
}

Polynom operator + (const Polynom &a, const Polynom &b)
{
    Polynom res(a);
    for (const Member &mem: b.addends)
        res.add(mem);
    return res;
}
