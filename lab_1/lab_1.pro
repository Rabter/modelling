TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        member.cpp \
        polynom.cpp

HEADERS += \
    member.h \
    polynom.h
