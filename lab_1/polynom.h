#ifndef POLYNOM_H
#define POLYNOM_H

#include <vector>
#include "member.h"

class Polynom
{
public:
    Polynom() = default;

    inline Member operator [] (unsigned i) const
    { return addends[i]; }

    friend Polynom operator + (const Polynom &a, const Polynom &b);

    void add(const Member &member);

    void integrate();
    void square();

    inline unsigned size() const
    { return addends.size(); }

private:
    std::vector<Member> addends;
};

#endif // POLYNOM_H
