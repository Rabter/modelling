#include <iostream>
#include <iomanip>
#include <cmath>
#include "polynom.h"
#define NUM_LEN 13
#define ETTA 0

long double picar_result(const Polynom &pol, double x)
{
    long double res = 0;
    for (unsigned i = 0; i < pol.size(); ++i)
        res += pow(x, pol[i].second) * pol[i].first;
    return res;
}

void get_polynom(Polynom &current, unsigned s)
{
    Member etta_member(ETTA, 0);
    Polynom base;
    base.add(Member(1, 1));
    base.add(etta_member);

    while (s)
    {
        Polynom adder(base);
        adder.square();
        current.square();
        current = current + adder;
        current.integrate();
        current.add(etta_member);
        --s;
    }
}

int main()
{
    unsigned s, reducer;
    double xmin = 0, xmax, xstep;
    std::cout << "Enter Picar's calculation depth:\n";
    std::cin >> s;

    Polynom poly_3, poly_4, poly_p;

    get_polynom(poly_3, 3);
    get_polynom(poly_4, 4);
    get_polynom(poly_p, s);

    std::cout << "Enter Xmax:\n";
    std::cin >> xmax;
    std::cout << "Enter x step:\n";
    std::cin >> xstep;
    std::cout << "Enter output reducer:\n";
    std::cin >> reducer;

    long double y_expl = ETTA, y_impl = ETTA;
    std::cout << std::setw(NUM_LEN) << std::left << "X" << "|" << std::setw(3 * NUM_LEN + 2) << std::left << "Picar" << "|" << std::setw(NUM_LEN) << std::left << "Explicit" << "|" << std::setw(NUM_LEN) << std::left << "Implicit" << std::endl;
    std::cout << std::setfill('-') << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN) << std::right << '-' << std::endl << std::setfill(' ');
    std::cout << std::setw(NUM_LEN + 1) << std::right << "|" << std::setw(NUM_LEN) << std::left << "3" << "|" << std::setw(NUM_LEN) << std::left << "4" << "|" << std::setw(NUM_LEN) << std::left << "p" << "|" << std::setw(NUM_LEN + 1) << std::right << "|" << std::endl;
    std::cout << std::setfill('-') << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN + 1) << std::right << '+' << std::setw(NUM_LEN) << std::right << '-' << std::endl << std::setfill(' ');
    double x = xmin;
    unsigned i = 0;
    while (x <= xmax)
    {
        if (!(i++ % reducer))
            std::cout << std::setw(NUM_LEN) << std::left << x << '|' << std::setw(NUM_LEN) << std::left << picar_result(poly_3, x) << '|' << std::setw(NUM_LEN) << std::left << picar_result(poly_4, x) << '|' << std::setw(NUM_LEN) << std::left << picar_result(poly_p, x) << '|' << std::setw(NUM_LEN) << std::left << y_expl << '|' << std::setw(NUM_LEN) << std::left << y_impl << std::endl;
        y_expl += xstep * (x * x + y_expl * y_expl);
        long double buf = 1 / (2 * xstep);
        x += xstep;
        y_impl = buf - sqrt(buf * buf - x * x - y_impl / xstep);
    }
}
