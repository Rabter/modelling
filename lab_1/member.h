#ifndef MEMBER_H
#define MEMBER_H

#include <utility>

class Member: public std::pair<double, int>
{
public:
    Member(double multiplier, int power);

    friend Member operator * (const Member &a, const Member &b);
};

#endif // MEMBER_H
