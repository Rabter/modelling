#include "member.h"

Member::Member(double multiplier, int power): std::pair<double, int>(multiplier, power) {}

Member operator * (const Member &a, const Member &b)
{
    return Member(a.first * b.first, a.second + b.second);
}

